from string import Template
from src.config.config import ETL_INPUT_TABLE, GCLOUD_PROJECT, BIGQUERY_DATASET_NAME

# query bariatrica
query_bariatrica = Template(
    f"""
    -- PROCEDIMENTOS DE INTERESSE REALIZADOS NOS 6 MESES ANTERIORES À DATA DE REFERÊNCIA.
CREATE OR REPLACE TEMP TABLE procedimentos AS
    SELECT DISTINCT idPessoa, idProcedimento, SexoBeneficiario
    FROM `{ETL_INPUT_TABLE}`
    WHERE
        idEmpresa = $id_empresa AND
        dtAtendimentoContaPaga < '$ref_date' AND
        dtAtendimentoContaPaga >= DATE_ADD('$ref_date', INTERVAL -6 MONTH);
        
        -----SELEÇÃO CRONICIDADES 
   CREATE OR REPLACE TEMP TABLE cronicidade AS
SELECT
  DISTINCT idPessoa,
  MAX(CronicoDiabetes_flag) AS diabetes,
  MAX(CronicoDislipidemia_flag) AS dislipidemia,
  MAX(CronicoOrtopedico_flag) AS ortopedia
FROM
  `{GCLOUD_PROJECT}.{BIGQUERY_DATASET_NAME}.view_fato_classificacao_vidas`
WHERE
  idEmpresa = $id_empresa
GROUP BY
  idPessoa;
SELECT
  a.idPessoa,
  a.idProcedimento,
  b.diabetes,
  b.dislipidemia,
  b.ortopedia,
  IF (sexoBeneficiario='Feminino',1,0) as sexo
FROM
  procedimentos a
INNER JOIN
  cronicidade b
ON
  a.idPessoa=b.idPessoa

    """
)

# query neonatal
query_neonatal = Template(
 f"""

   with  preditivos_inicial AS (
    SELECT DISTINCT idPessoa, idProcedimento
    FROM `fenix_bi.view_fato_sinistro`
    WHERE
        idEmpresa = $id_empresa  AND
        dtAtendimentoContaPaga < '$ref_date' AND
        dtAtendimentoContaPaga >= DATE_ADD('$ref_date', INTERVAL -12 MONTH)
           AND sexoBeneficiario !='Masculino'
            AND idadePessoa >=10),
    
    ------------------------------------------------------------------------------------------------------------
    
    -- SELEÇÃO DE GESTANTES
     gestantes_prever AS
    (SELECT distinct idPessoa, AltoCusto_Gestante
    FROM `fenix_bi.view_fato_classificacao_vidas`
    WHERE AltoCusto_Gestante = 1),
                
    ------------------------------------------------------------------------------------------------------------

    --SELEÇÃO DE IDADE
    
    idade AS (SELECT distinct idPessoa, MAX(idadePessoa) as idade 
    FROM fenix_bi.view_fato_sinistro 
    GROUP BY idPessoa)
    -----------------------------------------------------------------------------------------------------------



    SELECT pi.idPessoa, pi.idProcedimento,id.idade
    FROM preditivos_inicial AS pi
    INNER JOIN gestantes_prever AS gp
    ON pi.idPessoa = gp.idPessoa
    INNER JOIN idade as id
    on pi.idPessoa=id.idPessoa
    group by 
    pi.idPessoa,
     pi.idProcedimento,
     id.idade
    """
)


# query ortopedia
query_ortopedia = Template(
    f"""
    -- PROCEDIMENTOS DE INTERESSE REALIZADOS NOS 6 MESES ANTERIORES À DATA DE REFERÊNCIA.
    SELECT DISTINCT idPessoa, idProcedimento
    FROM `{ETL_INPUT_TABLE}`
    WHERE
        idEmpresa = $id_empresa AND
        dtAtendimentoContaPaga < '$ref_date' AND
        dtAtendimentoContaPaga >= DATE_ADD('$ref_date', INTERVAL -6 MONTH)
    """
)


# query bucomaxilo
query_bucomaxilo = Template(
    f"""
    -- PROCEDIMENTOS DE INTERESSE REALIZADOS NOS 6 MESES ANTERIORES À DATA DE REFERÊNCIA.
    SELECT DISTINCT idPessoa, idProcedimento
    FROM `{ETL_INPUT_TABLE}`
    WHERE
        idEmpresa = $id_empresa AND
        dtAtendimentoContaPaga < '$ref_date' AND
        dtAtendimentoContaPaga >= DATE_ADD('$ref_date', INTERVAL -6 MONTH)
    """
)

# query cardio
query_cardio = Template(
    f"""
    -- PROCEDIMENTOS DE INTERESSE REALIZADOS NOS 6 MESES ANTERIORES À DATA DE REFERÊNCIA.
    SELECT DISTINCT idPessoa, idProcedimento
    FROM `{ETL_INPUT_TABLE}`
    WHERE
        idEmpresa = $id_empresa AND
        dtAtendimentoContaPaga < '$ref_date' AND
        dtAtendimentoContaPaga >= DATE_ADD('$ref_date', INTERVAL -6 MONTH)
    """
)





